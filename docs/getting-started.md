# Getting Started with Taipy

## pip으로 설치

1. **사전 요구 사항**: Python(Version 3.8 이상)이 설치되어 있고 `pip`이 설치되어 있어야 한다.
1. **설치 명령**: 터미널 또는 명령 프롬프트에서 다음을 실행한다.

```
$ pip install taipy
```

다른 설치 방법이 필요하거나 Python 또는 pip이 설치가 안된 경우 [설치 페이지](https://docs.taipy.io/en/develop/installation/)를 참조하세요.

## 첫 Taipy Scenario
Taipy *Scenario*는 파이프라인 실행을 모델링한다. 작업이나 함수가 협업하고 데이터를 교환하는 실행 그래프라고 생각하면 된다. scenario를 얼마나 복잡하게 만들지는 사용자가 전적으로 몫이다.

기본적인 "Hello World" senario를 만들어 보겠다.

![](./images/hello_world.svg)

그래프는 다음을 포함한다.

- `input_name`이라는 입력 데이터 노드.
- `input_name` 데이터 노드를 처리하고 `message` 데이터 노드를 출력하는 함수 `build_message()`를 참조하는 작업.
- **구성**: 다음 Python 코드를 사용하여 실행 그래프를 설정한다.

```python
from taipy import Config


def build_message(name: str):
    return f"Hello {name}!"


input_name_data_node_cfg = Config.configure_data_node(id="input_name")
message_data_node_cfg = Config.configure_data_node(id="message")
build_msg_task_cfg = Config.configure_task("build_msg", build_message, input_name_data_node_cfg, message_data_node_cfg)
scenario_cfg = Config.configure_scenario("scenario", task_configs=[build_msg_task_cfg])
```

>    - `build_message`함수는 태스크가 실행 중에 사용할 함수를 정의한다.
>    - 변수 `input_name_data_node_cfg`와 `message_data_node_cfg`는 `input_name`와 `message` 데이터 노드를 각각 구성한다.
>    - `build_message()` 함수와 연결된 `build_msg`라는 태스크 `build_msg_task_cfg`를 구성하여 입력과 출력 데이터 노드를 지정한다.
>    - 마지막으로 이전에 구성한 작업들로 scenario `scenario_cfg`의 실행 그래프를 구성한다.

- **Core 서비스 초기화**: Core 서비스는 이전 단계의 구성을 처리하여 senario 관리 기능을 설정한다.

```python
from taipy import Core

if __name__ == "__main__":
    Core().run()
```

- senario와 데이터 관리: Core 서비스를 실행하면 scenario를 생성 및 관리하고, 실행을 위해 작업 그래프를 제출하고, 데이터 노드를 액세스할 수 있다.

```python
import taipy as tp

hello_scenario = tp.create_scenario(scenario_cfg)
hello_scenario.input_name.write("Taipy")
hello_scenario.submit()
print(hello_scenario.message.read())
```

 >  - 메서드 `tp.create_scenario()`는 이전에 빌드한 scenario 구성에서 새 scenario 이름 `hello_scenario`를 인스턴스화한다.
>   - `write()` 메서드를 사용하여 `hello_scenario`의 입력 데이터 노드 `input_name`에 문자열 값 `"Taipy"`를 설정한다.
>   - 실행을 위해 `hello_scenario`를 제출하여 작업의 생성과 실행을 트리거한다. 이 작업은 입력 데이터 노드를 읽고 값을 `build_message()` 함수에 전달한 다음 결과를 출력 데이터 노드에 쓴다.
>   - scenario `hello_scenario`를 실행하여 작성된 출력 데이터 노드 메시지를 읽고 디스플레이한다.

- 어플리케이션 실행:

    > 다음과 같은 Taipy CLI 명령으로 실행한다.
    > ```
    > $ taipy run hello_world_scenario.py
    > [2023-02-08 20:19:35,062][Taipy][INFO] job JOB_build_msg_9e5a5c28-6c3e-4b59-831d-fcc8b43f882e is completed.
    > Hello Taipy!
    > ```

위의 설명을 종합한 scenario 코드는 다음과 같다.

```python
import taipy as tp
from taipy import Config, Core, Gui


#            Configure application                             #
def build_message(name):
    return f"Hello {name}!"


# A first data node configuration to model an input name.
input_name_data_node_cfg = Config.configure_data_node(id="input_name")
# A second data node configuration to model the message to display.
message_data_node_cfg = Config.configure_data_node(id="message")
# A task configuration to model the build_message function.
build_msg_task_cfg = Config.configure_task("build_msg", build_message, input_name_data_node_cfg, message_data_node_cfg)
# The scenario configuration represents the whole execution graph.
scenario_cfg = Config.configure_scenario("scenario", task_configs=[build_msg_task_cfg])

#            Design graphical interface                        #

input_name = "이 윤준"
message = None


def submit_scenario(state):
    state.scenario.input_name.write(state.input_name)
    state.scenario.submit()
    state.message = scenario.message.read()

page = """
Name: <|{input_name}|input|>

<|submit|button|on_action=submit_scenario|>

Message: <|{message}|text|>
"""

if __name__ == "__main__":
    #            Instantiate and run Core service                  #
    Core().run()

    #            Manage scenarios and data nodes                   #
    scenario = tp.create_scenario(scenario_cfg)

    #            Instantiate and run Gui service                   #
    Gui(page).run()
```

## 그래픽 인터페이스 구축
scenario를 처리하기 위해 Taipy의 Python API를 사용했지만, 보다 사용자 친화적인 경험을 제공하기 위해 일반적으로 Taipy를 사용하여 만든 그래픽 인터페이스와도 원활하게 작동한다. 다음은 "Hello World" 시나리오를 위한 간단한 GUI 설정이다.

Taipy page는 마크다운, HTML, page의 마크다운 표현인 Python.`page` 등 여러 가지 방법으로 정의할 수 있다. 

**Markdown**

```python
import taipy as tp
from taipy import Config, Core, Gui


# Configure application                             
def build_message(name):
    return f"Hello {name}!"


# A first data node configuration to model an input name.
input_name_data_node_cfg = Config.configure_data_node(id="input_name")
# A second data node configuration to model the message to display.
message_data_node_cfg = Config.configure_data_node(id="message")
# A task configuration to model the build_message function.
build_msg_task_cfg = Config.configure_task("build_msg", build_message, input_name_data_node_cfg, message_data_node_cfg)
# The scenario configuration represents the whole execution graph.
scenario_cfg = Config.configure_scenario("scenario", task_configs=[build_msg_task_cfg])

# Design graphical interface                        
input_name = "Taipy"
message = None


def submit_scenario(state):
    state.scenario.input_name.write(state.input_name)
    state.scenario.submit()
    state.message = scenario.message.read()

page = """
Name: <|{input_name}|input|>

<|submit|button|on_action=submit_scenario|>

Message: <|{message}|text|>
"""

if __name__ == "__main__":
    # Instantiate and run Core service                  
    Core().run()

    # Manage scenarios and data nodes                   
    scenario = tp.create_scenario(scenario_cfg)

    # Instantiate and run Gui service                   
    Gui(page).run()
```

**Python**
```python
import taipy as tp
from taipy import Config, Core, Gui
import taipy.gui.builder as tgb


# Configure application 
def build_message(name):
    return f"Hello {name}!"


# A first data node configuration to model an input name.
input_name_data_node_cfg = Config.configure_data_node(id="input_name")
# A second data node configuration to model the message to display.
message_data_node_cfg = Config.configure_data_node(id="message")
# A task configuration to model the build_message function.
build_msg_task_cfg = Config.configure_task("build_msg", build_message, input_name_data_node_cfg, message_data_node_cfg)
# The scenario configuration represents the whole execution graph.
scenario_cfg = Config.configure_scenario("scenario", task_configs=[build_msg_task_cfg])

# Design graphical interface
input_name = "Taipy"
message = None


def submit_scenario(state):
    state.scenario.input_name.write(state.input_name)
    state.scenario.submit()
    state.message = scenario.message.read()

with tgb.Page() as page:
    tgb.text("Name:")
    tgb.input("{input_name}")
    tgb.button("Submit", on_action=submit_scenario)

    tgb.text("Message {message}")

if __name__ == "__main__":
    # Instantiate and run Core service
    Core().run()

    # Manage scenarios and data nodes 
    scenario = tp.create_scenario(scenario_cfg)

    # Instantiate and run Gui service
    Gui(page).run()
```

#### 시각적 요소
Taipy는 Python 변수 및 환경과 상호작용할 수 있는 다양한 시각적 요소를 제공한다. 이를 통해 변수를 표시하고, 변수를 수정하고, 어플리케이션과 상호 작용할 수 있다.

다음은 마크다운으로 `<|{variable}|visual_element_type|...|>` 또는 Python API로 `tgb.visual_element_type("{variable}", ...)`는 일반적인 예이다. `varibale`은 시각적 요소의 주요 속성이며, 일반적으로 시각적 요소를 통해 표시되거나 수정되는 것이다.

첫 예에서

- `input_name`은 텍스트 필드 입력에 바인딩되어 사용자의 입력이 `input_name` 변수에 직접 저장될 수 있다.
- `message`는 텍스트 필드에 연결되어 사용자에게 변경되는 콘텐츠를 디스플레이할 수 있다.

### Action을 통한 상호 작용
`on_action=submit_scenario`와 같은 action을 사용하면 버튼과 같은 시각적 요소가 특정 함수를 트리거할 수 있다.

```python
def submit_scenario(state):
    scenario.input_name.write(state.input_name)
    scenario.submit()
    state.message = scenario.message.read()
```

`submit_scenario()`를 포함한 모든 콜백은 첫 번째 파라미터로 [State](https://docs.taipy.io/en/develop/manuals/reference/taipy.gui.State) 객체를 받는다. 이 state는 사용자의 연결을 나타내며 사용자가 어플리케이션과 상호 작용하는 동안 변수를 읽고 설정하는 데 사용된다. 이를 통해 Taipy는 여러 사용자를 동시에 처리할 수 있다.

scenario는 누구나 사용할 수 있지만 `state.input_name`과 `state.input`은 상호작용하는 사용자에게만 해당된다. 이러한 설계를 통해 scenario와 같은 변수는 전역 변수인 반면 각 사용자의 action은 개별적이고 효율적으로 제어할 수 있다.

![](./images/state_illustration.png)

`submit_scenario()` 함수에서 사용자가 인터페이스에 입력한 `input_name`은 scenario에 저장된다. 제출 후 결과가 검색되어 메시지 변수에 저장된 다음 사용자 인터페이스에 디스플레이된다.

```python
if __name__ == "__main__":
    tp.Core().run()
    scenario = tp.create_scenario(scenario_cfg)
    tp.Gui(page).run()
```

어플리케이션의 핵심 부분은 Core 서비스를 설정하고, scenario를 생성하고, 인터페이스를 활성화하고 작동하게 하는 GUI를 시작하는 것으로 수행된다.

![](./images/result.png)

보다 현실적이고 고급 사용 사례는 [기술 자료](https://docs.taipy.io/en/develop/knowledge_base/) 또는 [페이지](https://docs.taipy.io/en/develop/manuals/)를 참조하세요.
