# Getting Started with Taipy <sup>[1](#footnote_1)</sup>

이 초보자를 위한 가이드를 통해 Taipy에 대해 알아보자. 첫 번째 어플리케이션을 쉽게 설치, 구성 및 생성하는 방법을 설명한다.

![](./images/result.png)

<a name="footnote_1">1</a>: [Getting Started](https://docs.taipy.io/en/develop/getting_started/)를 편역한 것이다.
